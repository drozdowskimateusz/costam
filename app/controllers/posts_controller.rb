class PostsController < ApplicationController

  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def edit
    
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      flash[:notice] = "Post stworzony"
      redirect_to posts_path
    else
      flash[:alert] = "Coś zjebałeś"
      render :new
    end
  end

  private

  def post_params
    params.require(:post).permit(:content, :title)
  end

end
